package com.homework.quiz.service;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertTrue;

class QuizServiceImplTest {
    private final ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
    private final QuizServiceImpl quizService = ctx.getBean(QuizServiceImpl.class);

    @BeforeAll
    static void setup() {
        System.out.println("QuizServiceImpl tests are running:");
    }

    @DisplayName("Checking csv resource:")
    @Test
    public void resourceTest() {
        ClassLoader classLoader = this.getClass().getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource("quiz.csv")).getFile());
        assertTrue(file.exists());
        System.out.println(file);
    }

    @DisplayName("Checking quiz queue:")
    @Test
    void queueCheck() {
        Assertions.assertNotNull(quizService);
        System.out.println(quizService.getQuizQueue());
    }

    @AfterAll
    public static void done() {
        System.out.println(" QuizServiceImpl tests finished");
    }

}