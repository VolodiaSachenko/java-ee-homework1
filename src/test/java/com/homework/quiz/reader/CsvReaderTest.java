package com.homework.quiz.reader;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class CsvReaderTest {

    @BeforeAll
    static void setup() {
        System.out.println("CsvReader tests are running:");
    }


    @DisplayName("Returning value testing")
    @Test
    void readCsvFile() {
        ClassLoader classLoader = this.getClass().getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource("quiz.csv")).getFile());
        List<List<String>> test = CsvReader.readCsvFile(file);
        assertFalse(test.isEmpty());
    }

    @AfterAll
    public static void done() {
        System.out.println("CsvReader tests finished");
    }

}