package com.homework.quiz;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootTest
class ApplicationTests {
    
    @BeforeAll
    static void setup() {
        System.out.println("Application tests are running:");
    }

    @DisplayName("QuizService bean test")
    @Test
    void contextLoads() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        Assertions.assertNotNull(ctx.getBean("quizService"));
    }

    @AfterAll
    public static void done() {
        System.out.println("Application tests finished");
    }
}