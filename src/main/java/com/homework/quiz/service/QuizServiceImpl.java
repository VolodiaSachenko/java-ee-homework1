package com.homework.quiz.service;

import com.homework.quiz.model.Quiz;
import com.homework.quiz.model.User;
import com.homework.quiz.reader.CsvReader;
import lombok.Getter;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.Scanner;

@Getter
public class QuizServiceImpl implements QuizService {
    private final Queue<Quiz> quizQueue = new LinkedList<>();
    private int score;

    public QuizServiceImpl(Resource resource) throws IOException {
        List<List<String>> csv = CsvReader.readCsvFile(resource.getFile());
        for (int i = 1; i < csv.size(); i++) {
            List<String> row = csv.get(i);
            List<String> answers = getAllAnswers(row);
            Quiz quiz = new Quiz(Integer.parseInt(row.get(0)), row.get(1), answers, Integer.parseInt(row.get(row.size() - 1)));
            this.quizQueue.add(quiz);
        }
    }

    public void run() {
        try (Scanner input = new Scanner(System.in)) {
            User user = identifyUser(input);
            System.out.println("Оберіть одну правильну відповідь за індексом від 1 до "
                    + Objects.requireNonNull(quizQueue.peek()).getAnswers().size() + ":");
            executeQuiz(quizQueue, input);
            System.out.println(user + ", правильних відповідей: " + score);
        } catch (InputMismatchException exception) {
            throw new IllegalArgumentException("Невірний формат. Оберіть одну правильну відповідь за індексом від 1 до "
                    + Objects.requireNonNull(quizQueue.peek()).getAnswers().size());
        }
    }

    private List<String> getAllAnswers(List<String> row) {
        List<String> answers = new ArrayList<>();
        for (int i = 2; i < row.size() - 1; i++) {
            answers.add(row.get(i));
        }
        return answers;
    }

    private User identifyUser(Scanner input) {
        System.out.println("Ваше ім'я:");
        String name = input.nextLine();

        System.out.println("Прізвище:");
        String sureName = input.nextLine();

        return new User(name, sureName);
    }

    private void executeQuiz(Queue<Quiz> quizQueue, Scanner input) {
        while (!quizQueue.isEmpty()) {
            Quiz active = quizQueue.poll();
            System.out.println(active);
            int userAnswer = input.nextInt();
            checkAnswer(userAnswer, active.getCorrect(), active.getAnswers().size());
        }
    }

    private void checkAnswer(int answer, int correct, int maxValue) {
        if (answer < 1 || answer > maxValue) {
            throw new IllegalArgumentException("Невірний індекс. Введіть індекс від 1 до " + maxValue);
        }
        if (answer == correct) {
            score++;
        }
    }
}