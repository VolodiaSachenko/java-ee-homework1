package com.homework.quiz.model;

import lombok.Getter;

@Getter
public class User {
    private final String name;
    private final String sureName;

    public User(String name, String sureName) {
        this.name = name;
        this.sureName = sureName;
    }

    @Override
    public String toString() {
        return name + " " + sureName;
    }
}
