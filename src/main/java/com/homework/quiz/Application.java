package com.homework.quiz;

import com.homework.quiz.service.QuizServiceImpl;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Volodymyr Sachenko
 */

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        QuizServiceImpl quiz = ctx.getBean(QuizServiceImpl.class);
        quiz.run();
    }

}
